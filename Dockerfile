FROM node:14-alpine

RUN apk --no-cache add bash curl jq git sed && npm install --global build-size

COPY pipe.sh /

RUN chmod a+x /pipe.sh

ENTRYPOINT ["/pipe.sh"]
