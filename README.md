# Bitbucket Pipelines Pipe: Add build size diff comment to pull request

This pipe will add a comment to your pull request with the build size diff using [build-size](https://www.npmjs.com/package/build-size).

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: swisnl/build-size-pipe:1
  variables:
    CLIENT_KEY: '<string>'
    CLIENT_SECRET: '<string>'
    PATTERNS: ['<string>']
    # DESTINATION_BRANCH: '<string>' # Optional.
    # DESTINATION_BRANCH_PREFIX: '<string>' # Optional.
    # DESTINATION_BRANCH_SUFFIX: '<string>' # Optional.
```

## Variables

| Variable                  | Usage                                                       |
| ------------------------- | ----------------------------------------------------------- |
| CLIENT_KEY (*)            | OAuth client id used to post a comment on your pull request. See [prerequisites](#markdown-header-prerequisites) |
| CLIENT_SECRET (*)         | OAuth client secret. |
| PATTERNS (*)              | Glob patterns to match build files. N.B. [hash] can be used as special matching pattern for hashed files. See [build-size](https://www.npmjs.com/package/build-size) for more information. |
| DESTINATION_BRANCH        | The name of the base branch to parse. Default: pull request target. |
| DESTINATION_BRANCH_PREFIX | A prefix for the pull request target branch (ignored when DESTINATION_BRANCH is provided). Default: empty. |
| DESTINATION_BRANCH_SUFFIX | A suffix for the pull request target branch (ignored when DESTINATION_BRANCH is provided). Default: empty. |

_(*) = required variable._

## Prerequisites

To use this pipe, you need a (private) OAuth consumer key and secret with pull requests read permission. You can follow the instructions [here](https://support.atlassian.com/bitbucket-cloud/docs/push-back-to-your-repository/#OAuth) to create one.

## Examples

Basic example:

```yaml
script:
  - pipe: swisnl/build-size-pipe:1
    variables:
      CLIENT_KEY: $BUILD_SIZE_PIPE_CLIENT_KEY
      CLIENT_SECRET: $BUILD_SIZE_PIPE_CLIENT_SECRET
      PATTERNS: ["public/**/*.css", "public/**/*.js"]
```

Advanced example (dedicated build branch):

```yaml
script:
  - pipe: swisnl/build-size-pipe:1
    variables:
      CLIENT_KEY: $BUILD_SIZE_PIPE_CLIENT_KEY
      CLIENT_SECRET: $BUILD_SIZE_PIPE_CLIENT_SECRET
      PATTERNS: ["public/**/*.css", "public/**/*.js"]
      DESTINATION_BRANCH: "build"
```

Advanced example (prefix/suffix build branch):

```yaml
script:
  - pipe: swisnl/build-size-pipe:1
    variables:
      CLIENT_KEY: $BUILD_SIZE_PIPE_CLIENT_KEY
      CLIENT_SECRET: $BUILD_SIZE_PIPE_CLIENT_SECRET
      PATTERNS: ["public/**/*.css", "public/**/*.js"]
      DESTINATION_BRANCH_PREFIX: "dist/"
      DESTINATION_BRANCH_SUFFIX: "--build"
```

## Support

If you’d like help with this pipe, or you have an issue or feature request, let us know. The pipe is maintained by info@swis.nl.

If you're reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce
