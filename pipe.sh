#!/usr/bin/env bash

set -e

# Functions.
init_array_var() {
    local array_var=${1}
    local count_var=${array_var}_COUNT
    for (( i = 0; i < ${!count_var:=0}; i++ ))
    do
      eval ${array_var}[$i]='$'${array_var}_${i}
    done
}

# Required parameters.
BITBUCKET_PR_ID="${BITBUCKET_PR_ID:?'This pipe can only be used on pull requests.'}"
DESTINATION_BRANCH="${DESTINATION_BRANCH:="${DESTINATION_BRANCH_PREFIX:=""}${BITBUCKET_PR_DESTINATION_BRANCH}${DESTINATION_BRANCH_SUFFIX}"}"
PATTERNS[0]=${PATTERNS_0:?'PATTERNS environment variable missing.'}
init_array_var "PATTERNS"
CLIENT_KEY=${CLIENT_KEY:?'CLIENT_KEY environment variable missing.'}
CLIENT_SECRET=${CLIENT_SECRET:?'CLIENT_SECRET environment variable missing.'}

# Default parameters.
TMP_DIR=${TMP_DIR:="/tmp/build-size"}

# Create a temporary directory.
mkdir -p ${TMP_DIR}

# Setup proxy for origin.
git config "http.${BITBUCKET_GIT_HTTP_ORIGIN}.proxy" "http://host.docker.internal:29418/"
git remote set-url origin "${BITBUCKET_GIT_HTTP_ORIGIN}"

# Check if the origin remote is configured to track the destination branch.
# If not, configure it to do so.
# Bitbucket pipelines only clones the branch the pipeline is applied to and
# only with a default depth of 50.
if ! git show-ref --verify --quiet "refs/remotes/origin/${DESTINATION_BRANCH}"; then
  git config --add remote.origin.fetch "+refs/heads/${DESTINATION_BRANCH}:refs/remotes/origin/${DESTINATION_BRANCH}"
fi

# Fetch and checkout the destination branch using a working tree.
if ! git fetch origin "${DESTINATION_BRANCH}"; then
  echo "Destination branch not found, could not compare build sizes."
  exit 0
fi
git worktree add -b "${DESTINATION_BRANCH}" "${TMP_DIR}/destination/" "origin/${DESTINATION_BRANCH}"

# Parse build sizes.
build-size parse "${PATTERNS[@]}" > ${TMP_DIR}/source.json
cd "${TMP_DIR}/destination/"
build-size parse "${PATTERNS[@]}" > ${TMP_DIR}/destination.json
cd "${BITBUCKET_CLONE_DIR}"

# Cleanup working tree.
git worktree remove -f "${TMP_DIR}/destination/"

# Compare build sizes.
# BitBucket does not need escaped slashes (\/) for some reason,
# so we unescape those with sed.
COMMENT=$(build-size compare ${TMP_DIR}/destination.json ${TMP_DIR}/source.json --exclude-unchanged --omit-output-if-equal --disable-images | sed -e 's/\\\//\//g')

# If the builds do not differ, exit and do not post a comment.
if [ -z "${COMMENT}" ]; then
  echo "Builds do not differ."
  exit 0
fi

# Fetch an access token.
ACCESS_TOKEN=$(curl -s -X POST -u "${CLIENT_KEY}:${CLIENT_SECRET}" \
  https://bitbucket.org/site/oauth2/access_token \
  -d grant_type=client_credentials -d scopes="pullrequest" | jq --raw-output '.access_token')

# Post a comment on the pull request.
curl -f -sS -X POST -H "Authorization: Bearer ${ACCESS_TOKEN}" -H "Content-Type: application/json" \
  "https://api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_FULL_NAME}/pullrequests/${BITBUCKET_PR_ID}/comments" \
  -d "$(jq -n --compact-output --arg content "${COMMENT}" '{content: {raw: $content}}')"

echo
echo "Comment posted."
